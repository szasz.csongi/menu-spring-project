package edu.bbte.idde.scim1957.backend.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class BaseEntity {
    Long id;
}
