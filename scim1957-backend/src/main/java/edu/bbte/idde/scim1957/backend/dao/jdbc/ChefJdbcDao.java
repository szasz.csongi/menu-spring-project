package edu.bbte.idde.scim1957.backend.dao.jdbc;

import edu.bbte.idde.scim1957.backend.dao.ChefDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.dao.jdbc.config.DataSourceFactory;
import edu.bbte.idde.scim1957.backend.model.Chef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ChefJdbcDao implements ChefDao {
    private static final Logger LOG = LoggerFactory.getLogger(ChefJdbcDao.class);
    private Connection connection;

    @Override
    public List<Chef> findByMenu(Long id) throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            List<Chef> chefs = new ArrayList<>();
            try {
                LOG.info("findByMenu {} - ChefJdbcDao", id.toString());
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,name,menuid FROM chefs WHERE menuid=?");

                preSta.setLong(1,id);
                ResultSet set = preSta.executeQuery();
                if (set.next()) {
                    Chef chef = createChef(set);
                    chefs.add(chef);

                }
            } catch (SQLException e) {
                LOG.error("error in find by Menu");
                throw new RepositoryException("Couldn't find entity in menus", e);
            }
            return chefs;
        } catch (SQLException e) {
            LOG.error("connection was not successful");
            throw new RepositoryException("connection was not successful");
        }
    }

    @Override
    public void create(Chef chef) throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("create {} - ChefJdbcDao", chef.toString());
                PreparedStatement preSta = connection.prepareStatement(
                        "INSERT into chefs (name,menuid) VALUES (?,?)");
                createChefToPreparedStatement(preSta,chef);
                preSta.executeUpdate();
            } catch (SQLException e) {
                LOG.error("Error in create");
                throw new RepositoryException("ChefJdbcDao create failed", e);
            }
        } catch (SQLException e) {
            LOG.error("connection was not successful");
            throw new RepositoryException("connection was not successful");
        }
    }

    @Override
    public Collection<Chef> findAll() throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            Collection<Chef> chefs = new ArrayList<>();
            try {
                LOG.info("findAll - ChefJdbcDao");
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,name,menuid FROM chefs");
                ResultSet set = preSta.executeQuery();
                while (set.next()) {
                    Chef chef = createChef(set);
                    chefs.add(chef);
                }
            } catch (SQLException e) {
                LOG.error("error in findall");
                throw new RepositoryException("ChefJdbcDao findall failed", e);
            }

            return chefs;
        } catch (SQLException e) {
            LOG.error("connection was not successful");
            throw new RepositoryException("connection was not successful");
        }
    }

    @Override
    public Chef findById(Long id) throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("findById {} - ChefJdbcDao", id.toString());
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,name,menuid FROM chefs WHERE id=?");

                preSta.setLong(1,id);
                ResultSet set = preSta.executeQuery();
                if (set.next()) {
                    return createChef(set);
                }
            } catch (SQLException e) {
                LOG.error("Error in find by id");
                throw new RepositoryException("ChefJdbcDao could not find chef with given id", e);
            }
        } catch (SQLException e) {
            LOG.error("connection was not successful");
            throw new RepositoryException("connection was not successful");
        }
        return null;
    }

    @Override
    public void delete(Long id) throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("delete {} - ChefJdbcDao", id.toString());
                PreparedStatement preSta =
                        connection.prepareStatement("DELETE FROM chefs WHERE id=?");

                preSta.setLong(1,id);
                preSta.executeUpdate();

            } catch (SQLException e) {
                LOG.error("Error in delete");
                throw new RepositoryException("ChefJdbcDao delete failed", e);
            }
        } catch (SQLException e) {
            LOG.error("connection was not successful");
            throw new RepositoryException("connection was not successful");
        }
    }

    @Override
    public void update(Long id, Chef chef) throws SQLException, RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("update {} - ChefJdbcDao", id.toString());
                PreparedStatement preSta =
                        connection.prepareStatement("UPDATE chefs SET name=?, menuid=? WHERE id=?");
                updateChefToPreparedStatement(preSta,id,chef);
                preSta.executeUpdate();
            }  catch (SQLException e) {
                LOG.error("Error in update");
                throw new RepositoryException("ChefJdbcDao update failed", e);
            }
        } catch (SQLException e) {
            LOG.error("connection was not successful");
            throw new RepositoryException("connection was not successful");
        }
    }

    private Chef createChef(ResultSet set) throws SQLException {
        Chef chef = new Chef(set.getString(2),
                set.getLong(3));
        chef.setId(set.getLong(1));
        return chef;
    }

    private void createChefToPreparedStatement(PreparedStatement preSta, Chef chef) throws SQLException {
        preSta.setString(1, chef.getName());
        preSta.setLong(2, chef.getMenuid());
    }

    private void updateChefToPreparedStatement(PreparedStatement preSta, Long id, Chef chef) throws SQLException {
        preSta.setString(1, chef.getName());
        preSta.setLong(2, chef.getMenuid());
        preSta.setLong(3,id);
    }
}
