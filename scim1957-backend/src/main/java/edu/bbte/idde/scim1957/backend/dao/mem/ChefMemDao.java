package edu.bbte.idde.scim1957.backend.dao.mem;

import edu.bbte.idde.scim1957.backend.dao.ChefDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.model.Chef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class ChefMemDao implements ChefDao {
    private final ConcurrentHashMap<Long, Chef> chefs;
    private final AtomicLong id;
    static final Logger LOGGER = LoggerFactory.getLogger(MenuMemDao.class);

    public ChefMemDao() {
        chefs = new ConcurrentHashMap<>();
        id = new AtomicLong();
        LOGGER.info("chefMemDao");
    }

    @Override
    public List<Chef> findByMenu(Long id) throws RepositoryException {
        LOGGER.info("sepciific chefs from database found");
        return chefs.values().stream()
                .filter(chef -> chef.getId().toString().equals(id.toString()))
                .collect(Collectors.toList());
    }

    @Override
    public void create(Chef chef) throws RepositoryException {
        Long id = this.id.incrementAndGet();
        chef.setId(id);
        this.chefs.put(id,chef);
        LOGGER.info("Chef {} was created", chef);
    }

    @Override
    public Collection<Chef> findAll() throws RepositoryException {
        LOGGER.info("All chefs from database were found");
        return chefs.values();
    }

    @Override
    public Chef findById(Long id) throws RepositoryException {
        Chef chef = chefs.get(id);
        LOGGER.info("Chef with id {} was found: {}", id, chef);
        return  chef;
    }

    @Override
    public void delete(Long id) throws RepositoryException {
        chefs.remove(id);
        LOGGER.info("Chef with id {} was deleted", id);
    }

    @Override
    public void update(Long id, Chef chef) throws SQLException, RepositoryException {
        chef.setId(id);
        chefs.put(id, chef);
        LOGGER.info("Chef with id {} was updated to {}", id, chef);
    }
}
