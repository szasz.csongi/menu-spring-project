package edu.bbte.idde.scim1957.backend.dao;

import edu.bbte.idde.scim1957.backend.model.Chef;

import java.sql.SQLException;
import java.util.List;

public interface ChefDao extends Dao<Chef> {
    List<Chef> findByMenu(Long id) throws RepositoryException;

    static ChefDao getChefDao() throws RepositoryException, SQLException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        return daoFactory.getChefDao();
    }
}
