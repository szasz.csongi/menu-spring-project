package edu.bbte.idde.scim1957.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Menu extends BaseEntity {

    private Integer price;
    private String name;
    private Double calories;
    private Boolean glutenFree;
    private Boolean vegan;

    public Menu readValue(Long id, Integer price,String name, Double calories, Boolean glutenFree, Boolean vegan) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.calories = calories;
        this.glutenFree = glutenFree;
        this.vegan = vegan;
        return this;
    }
}
