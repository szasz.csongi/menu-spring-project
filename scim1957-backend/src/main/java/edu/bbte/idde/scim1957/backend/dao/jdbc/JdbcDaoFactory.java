package edu.bbte.idde.scim1957.backend.dao.jdbc;

import edu.bbte.idde.scim1957.backend.dao.ChefDao;
import edu.bbte.idde.scim1957.backend.dao.DaoFactory;
import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;

import java.sql.SQLException;

public class JdbcDaoFactory extends DaoFactory {
    private static MenuDao menuDao;
    private static ChefDao chefDao;

    @Override
    public synchronized MenuDao getMenuDao() throws SQLException, RepositoryException {
        if (menuDao == null) {
            this.menuDao = new MenuJdbcDao();
        }
        return menuDao;
    }

    @Override
    public synchronized ChefDao getChefDao() throws SQLException, RepositoryException {
        if (chefDao == null) {
            this.chefDao = new ChefJdbcDao();
        }
        return chefDao;
    }
}
