package edu.bbte.idde.scim1957.backend.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Config {
    private String daoType;
    private String username;
    private String password;
    private String jdbcUrl;
    private String dbUseSSL;
    private Integer connectionPoolSize;
}
