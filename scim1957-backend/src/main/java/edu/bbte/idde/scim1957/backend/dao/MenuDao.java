package edu.bbte.idde.scim1957.backend.dao;

import edu.bbte.idde.scim1957.backend.model.Menu;

import java.sql.SQLException;
import java.util.List;

//import java.util.Collection;

public interface MenuDao extends Dao<Menu> {
    @Override
    void create(Menu menu);

    List<Menu> findByName(String name) throws RepositoryException;

    static  Dao<Menu> getMenuDao() throws RepositoryException, SQLException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        return daoFactory.getMenuDao();
    }
}
