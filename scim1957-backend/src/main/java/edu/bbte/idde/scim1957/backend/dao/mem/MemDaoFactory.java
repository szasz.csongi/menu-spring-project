package edu.bbte.idde.scim1957.backend.dao.mem;

import edu.bbte.idde.scim1957.backend.dao.ChefDao;
import edu.bbte.idde.scim1957.backend.dao.DaoFactory;
import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;

import java.sql.SQLException;

public class MemDaoFactory extends DaoFactory {
    private static MenuDao menuDao;
    private static ChefDao chefDao;

    @Override
    public synchronized MenuDao getMenuDao() {
        if (menuDao == null) {
            menuDao = new MenuMemDao();
        }
        return menuDao;
    }

    @Override
    public synchronized ChefDao getChefDao() throws SQLException, RepositoryException {
        if (chefDao == null) {
            chefDao = new ChefMemDao();
        }
        return chefDao;
    }
}
