package edu.bbte.idde.scim1957.backend.dao.jdbc.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import edu.bbte.idde.scim1957.backend.config.Config;
import edu.bbte.idde.scim1957.backend.config.ConfigFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceFactory {
    private static HikariDataSource DATA_SOURCE;

    public static synchronized Connection getConnection() throws SQLException {
        if (DATA_SOURCE == null) {
            DATA_SOURCE = createDatasource();
        }
        return DATA_SOURCE.getConnection();
    }

    private static HikariDataSource createDatasource() {
        final HikariConfig hikariConfig = new HikariConfig();
        Config config = ConfigFactory.getConfig();
        hikariConfig.setJdbcUrl(config.getJdbcUrl());
        hikariConfig.setUsername(config.getUsername());
        hikariConfig.setPassword(config.getPassword());
        hikariConfig.setDriverClassName("com.mysql.jdbc.Driver");
        hikariConfig.addDataSourceProperty("useSSL", config.getDbUseSSL());
        hikariConfig.setMaximumPoolSize(config.getConnectionPoolSize());

        return new HikariDataSource(hikariConfig);
    }
}
