package edu.bbte.idde.scim1957.backend.dao;

import edu.bbte.idde.scim1957.backend.model.BaseEntity;

import java.sql.SQLException;
import java.util.Collection;

public interface Dao<T extends BaseEntity> {
    void  create(T entity) throws RepositoryException;

    Collection<T> findAll() throws RepositoryException;

    T findById(Long id) throws RepositoryException;

    void delete(Long id) throws RepositoryException;

    void update(Long id, T entity) throws SQLException, RepositoryException;
}
