package edu.bbte.idde.scim1957.backend.dao;

import edu.bbte.idde.scim1957.backend.config.Config;
import edu.bbte.idde.scim1957.backend.config.ConfigFactory;
import edu.bbte.idde.scim1957.backend.dao.jdbc.JdbcDaoFactory;
import edu.bbte.idde.scim1957.backend.dao.mem.MemDaoFactory;

import java.sql.SQLException;

public abstract class DaoFactory {
    private static DaoFactory instance;

    public static synchronized DaoFactory getInstance() throws RepositoryException {
        if (instance == null) {
            Config config = ConfigFactory.getConfig();
            if ("jdbc".equals(config.getDaoType())) {
                instance = new JdbcDaoFactory();
            } else if ("mem".equals(config.getDaoType())) {
                instance = new MemDaoFactory();
            } else {
                instance = new MemDaoFactory();
                throw new RepositoryException("DAO type is not supported");
            }
        }
        return instance;
    }

    public abstract MenuDao getMenuDao() throws SQLException, RepositoryException;

    public abstract ChefDao getChefDao() throws SQLException, RepositoryException;
}
