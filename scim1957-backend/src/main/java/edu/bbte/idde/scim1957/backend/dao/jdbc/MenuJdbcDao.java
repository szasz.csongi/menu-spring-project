package edu.bbte.idde.scim1957.backend.dao.jdbc;

import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.dao.jdbc.config.DataSourceFactory;
import edu.bbte.idde.scim1957.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MenuJdbcDao implements MenuDao {
    private static final Logger LOG = LoggerFactory.getLogger(MenuJdbcDao.class);
    private Connection connection;

    @Override
    public void create(Menu menu) {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("create {} - MenuJdbcDao", menu.toString());
                PreparedStatement preSta = connection.prepareStatement(
                        "INSERT into menus (price,name,calories,glutenFree,vegan) VALUES (?,?,?,?,?)");
                createMenuToPreparedStatement(preSta,menu);
                preSta.executeUpdate();
            } catch (SQLException e) {
                LOG.error("MenuJdbcDao create failed");
            }
        } catch (SQLException e) {
            LOG.error("Connecion was not successful");
        }
    }

    @Override
    public Collection<Menu> findAll() throws RepositoryException {
        Collection<Menu> menus = new ArrayList<>();
        try {
            connection = DataSourceFactory.getConnection();

            try {
                LOG.info("findall - MenuJdbcDao");
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,price,name,calories,glutenFree,vegan FROM menus");
                ResultSet set = preSta.executeQuery();
                while (set.next()) {
                    Menu menu = createEntity(set);
                    menus.add(menu);
                }
                return menus;
            } catch (SQLException e) {
                LOG.error("Error in findall");
                throw new RepositoryException("Failed to find entities in menus", e);
            }

        } catch (SQLException e) {
            LOG.error("Connecion was not successful");
        }
        return menus;
    }

    @Override
    public Menu findById(Long id) throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("findById {} - MenuJdbcDao", id);
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,price,name,calories,glutenFree"
                                + ",vegan FROM menus WHERE id=?");

                preSta.setLong(1,id);
                ResultSet set = preSta.executeQuery();
                if (set.next()) {
                    return createEntity(set);
                }
            } catch (SQLException e) {
                throw new RepositoryException("Couldn't find entity in menus", e);
            }
        } catch (SQLException e) {
            LOG.error("Connecion was not successful");
        }
        return null;
    }

    @Override
    public void delete(Long id) throws RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("delete {} - MenuJdbcDao", id);
                PreparedStatement preSta =
                        connection.prepareStatement("DELETE FROM menus WHERE id=?");

                preSta.setLong(1,id);
                preSta.executeUpdate();

            } catch (SQLException e) {
                LOG.error("Error in delete");
                throw new RepositoryException("Failed to delete entity in menus", e);
            }
        } catch (SQLException e) {
            LOG.error("Connecion was not successful");
        }
    }

    @Override
    public void update(Long id, Menu menu) throws SQLException, RepositoryException {
        try {
            connection = DataSourceFactory.getConnection();
            try {
                LOG.info("update {} - MenuJdbcDao", id);
                PreparedStatement preSta =
                        connection.prepareStatement("UPDATE menus SET price=?, name=?, calories=?, "
                                + "glutenFree=?, vegan=? WHERE id=?");
                updateMenuToPreparedStatement(preSta,id,menu);
                preSta.executeUpdate();
            }  catch (SQLException e) {
                LOG.error("Error in update");
                throw new RepositoryException("Failed to update entity in menus", e);
            }
        } catch (SQLException e) {
            LOG.error("Connecion was not successful");
        }
    }

    @Override
    public List<Menu> findByName(String name) throws RepositoryException {
        List<Menu> menus = new ArrayList<>();
        try {
            connection = DataSourceFactory.getConnection();
            LOG.info("Connection was successful");
            try {
                LOG.info("findByName {} - MenuJdbcDao", name);
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,price,name,calories,glutenFree,vegan"
                                + " FROM menus WHERE name=?");
                preSta.setString(1,name);
                ResultSet set = preSta.executeQuery();
                if (set.next()) {
                    Menu menu = createEntity(set);

                    menus.add(menu);
                }
            } catch (SQLException e) {
                LOG.error("Error in find by name");
                throw new RepositoryException("Failed to find entity in menus", e);
            }
            return menus;
        } catch (SQLException e) {
            LOG.error("Connecion was not successful");
        }
        return menus;
    }

    private Menu createEntity(ResultSet set) throws SQLException {
        Menu menu = new Menu(set.getInt(2), set.getString(3),
                set.getDouble(4), set.getBoolean(5), set.getBoolean(6));
        menu.setId(set.getLong(1));
        return menu;
    }

    private void createMenuToPreparedStatement(PreparedStatement preSta, Menu menu) throws SQLException {
        preSta.setInt(1, menu.getPrice());
        preSta.setString(2, menu.getName());
        preSta.setDouble(3, menu.getCalories());
        preSta.setBoolean(4, menu.getGlutenFree());
        preSta.setBoolean(5, menu.getVegan());
    }

    private void updateMenuToPreparedStatement(PreparedStatement preSta, Long id, Menu menu) throws SQLException {
        preSta.setInt(1, menu.getPrice());
        preSta.setString(2, menu.getName());
        preSta.setDouble(3, menu.getCalories());
        preSta.setBoolean(4, menu.getGlutenFree());
        preSta.setBoolean(5, menu.getVegan());
        preSta.setLong(6,id);
    }
}
