package edu.bbte.idde.scim1957.backend.dao.mem;

import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class MenuMemDao implements MenuDao {
    private final ConcurrentHashMap<Long, Menu> menus;
    private final AtomicLong id;
    static final Logger LOGGER = LoggerFactory.getLogger(MenuMemDao.class);

    public MenuMemDao() {
        menus = new ConcurrentHashMap<>();
        id = new AtomicLong();
        LOGGER.info("menuMemDao");
    }

    @Override
    public void create(Menu menu) {
        Long id = this.id.incrementAndGet();
        menu.setId(id);
        this.menus.put(id,menu);
        LOGGER.info("menu {} was created", menu);
    }

    @Override
    public Menu findById(Long id) {
        Menu menu = menus.get(id);
        LOGGER.info("Menu with id {} was found: {}", id, menu);
        return  menu;
    }

    @Override
    public void update(Long id, Menu menu) {
        menu.setId(id);
        menus.put(id, menu);
        LOGGER.info("Menu with id {} was updated to {}", id, menu);
    }

    @Override
    public void delete(Long id) {
        menus.remove(id);
        LOGGER.info("Menu with id {} was deleted", id);
    }

    @Override
    public Collection<Menu> findAll() {
        LOGGER.info("All menus from database were found");
        return menus.values();
    }

    @Override
    public List<Menu> findByName(String name) throws RepositoryException {
        LOGGER.info("finding menu by name {}", name);
        return menus.values().stream()
                .filter(menu -> menu.getName().equals(name))
                .collect(Collectors.toList());
    }
}
