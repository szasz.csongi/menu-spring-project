package edu.bbte.idde.scim1957.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.scim1957.backend.dao.ChefDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.dao.mem.ChefMemDao;
import edu.bbte.idde.scim1957.backend.model.Chef;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

@WebServlet("/chefs")
public class ChefServlet extends HttpServlet {
    private ChefDao dao;
    private ObjectMapper objectMapper;
    public static final Logger LOG = LoggerFactory.getLogger(ChefMemDao.class);

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            dao = (ChefDao) ChefDao.getChefDao();
        } catch (RepositoryException | SQLException e) {
            LOG.info("Failed init");
        }
        objectMapper = ObjectMapperFactory.getObjectMapper();
        LOG.info("Servlet initalized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("GET chefs");
        resp.setHeader("Content-type", "application/json");
        if (req.getParameter("id") == null) {
            Collection<Chef> chefs = null;
            try {
                chefs = dao.findAll();
            } catch (RepositoryException e) {
                LOG.error("Get error, rep");
            }
            objectMapper.writeValue(resp.getWriter(), chefs);
            return;
        }

        try {
            Long id = Long.parseLong(req.getParameter("id"));
            Chef chef = null;
            try {
                chef = dao.findById(id);
            } catch (RepositoryException e) {
                LOG.error("Get error, rep");
            }
            if (chef == null) {
                resp.sendError(404);
                objectMapper.writeValue(resp.getWriter(), "There is no chef with this Id");
                return;
            }
            objectMapper.writeValue(resp.getWriter(), chef);
        } catch (NumberFormatException e) {
            resp.sendError(400);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Post a chef");
        resp.setHeader("Content-Type", "application/json");
        try {
            Chef chef = objectMapper.readValue(req.getInputStream(), Chef.class);
            try {
                dao.create(chef);
            } catch (RepositoryException e) {
                LOG.error("Post error, rep");
            }
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            try {
                objectMapper.writeValue(resp.getWriter(), dao.findAll());
            } catch (RepositoryException e) {
                LOG.error("Post error, rep");
            }
            return;
        }  catch (IOException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");
        }  catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");

            return;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Delete a chef");
        resp.setHeader("Content-type", "application/json");
        if (req.getParameter("id") == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            objectMapper.writeValue(resp.getWriter(), "No chef with this id");
            return;
        }

        try {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                dao.delete(id);
            } catch (RepositoryException e) {
                LOG.error("Delete error, rep");
            }
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            LOG.info("Chef was deleted");
            return;
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");
            return;
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Modify a chef");
        try {
            String id = req.getParameter("id");
            Chef chef = objectMapper.readValue(req.getInputStream(), Chef.class);

            LOG.info(id);
            LOG.info(chef.toString());

            try {
                dao.update(Long.parseLong(id), chef);
            } catch (SQLException throwables) {
                LOG.error("Put error, sql");
            } catch (RepositoryException e) {
                LOG.error("Put error, rep");
            }
            return;

        } catch (IOException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");

            return;
        }
    }
}
