package edu.bbte.idde.scim1957.web;

import edu.bbte.idde.scim1957.backend.dao.mem.MenuMemDao;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final String USERNAME = "user";
    private static final String PASSWORD = "secret";
    private  static final Logger LOG = LoggerFactory.getLogger(MenuMemDao.class);

    @Override
    public void init() throws ServletException {
        super.init();
        LOG.info("Initialising Login servlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("login.html").forward(req, resp);
        LOG.info("Get login page");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (USERNAME.equals(req.getParameter("username")) && PASSWORD.equals(req.getParameter("password"))) {
            req.getSession().setAttribute("username", req.getParameter("username"));
            req.getSession().setAttribute("password", req.getParameter("password"));
            resp.sendRedirect(req.getContextPath().concat("/templateMenu"));
        } else {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
        LOG.info("Post on login");
    }
}
