package edu.bbte.idde.scim1957.web;

import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/templateMenu")
public class TemplateServlet extends HttpServlet {
    private MenuDao dao;
    private static final Logger LOG = LoggerFactory.getLogger(MenuServlet.class);

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            dao = (MenuDao) MenuDao.getMenuDao();
        } catch (RepositoryException e) {
            LOG.info("Init error, rep");
        } catch (SQLException throwables) {
            LOG.info("Init error, sql");
        }
        LOG.info("Initializing menu template");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("GET template");
        try {
            req.setAttribute("menu", dao.findAll());
        } catch (RepositoryException e) {
            LOG.info("Get error");
        }
        req.getRequestDispatcher("show-menus.jsp").forward(req, resp);
    }
}
