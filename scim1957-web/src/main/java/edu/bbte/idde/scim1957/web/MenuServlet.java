package edu.bbte.idde.scim1957.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.dao.mem.MenuMemDao;
import edu.bbte.idde.scim1957.backend.model.Menu;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;

@WebServlet("/menus")
public class MenuServlet extends HttpServlet {
    private MenuDao dao;
    private ObjectMapper objectMapper;
    public static final Logger LOG = LoggerFactory.getLogger(MenuMemDao.class);

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            dao = (MenuDao) MenuDao.getMenuDao();
        } catch (RepositoryException | SQLException e) {
            LOG.info("Failed init");
        }
        objectMapper = ObjectMapperFactory.getObjectMapper();
        LOG.info("Servlet initalized");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("GET menus");
        resp.setHeader("Content-type", "application/json");
        if (req.getParameter("id") == null) {
            Collection<Menu> menus = null;
            try {
                menus = dao.findAll();
            } catch (RepositoryException e) {
                LOG.error("Get error, rep");
            }
            objectMapper.writeValue(resp.getWriter(), menus);
            return;
        }

        try {
            Long id = Long.parseLong(req.getParameter("id"));
            Menu menu = null;
            try {
                menu = dao.findById(id);
            } catch (RepositoryException e) {
                LOG.error("Get error, rep");
            }
            if (menu == null) {
                resp.sendError(404);
                objectMapper.writeValue(resp.getWriter(), "There is no menu with this Id");
                return;
            }
            objectMapper.writeValue(resp.getWriter(), menu);
        } catch (NumberFormatException e) {
            resp.sendError(400);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Post a menu");
        resp.setHeader("Content-Type", "application/json");
        try {
            Menu menu = objectMapper.readValue(req.getInputStream(), Menu.class);
            dao.create(menu);
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            try {
                objectMapper.writeValue(resp.getWriter(), dao.findAll());
            } catch (RepositoryException e) {
                LOG.error("Post error, rep");
            }
            return;
        }  catch (IOException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");
        }  catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");

            return;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Delete a menu");
        resp.setHeader("Content-type", "application/json");
        if (req.getParameter("id") == null) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            objectMapper.writeValue(resp.getWriter(), "No menu with this id");
            return;
        }

        try {
            Long id = Long.parseLong(req.getParameter("id"));
            try {
                dao.delete(id);
            } catch (RepositoryException e) {
                LOG.error("Delete error, rep");
            }
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
            LOG.info("Menu was deleted");
            return;
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");
            return;
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("Modify a menu");
        try {
            String id = req.getParameter("id");
            Menu menu = objectMapper.readValue(req.getInputStream(), Menu.class);

            LOG.info(id);
            LOG.info(menu.toString());

            try {
                dao.update(Long.parseLong(id), menu);
            } catch (SQLException throwables) {
                LOG.error("Put error, sql");
            } catch (RepositoryException e) {
                LOG.error("Put error, rep");
            }
            return;

        } catch (IOException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");
        } catch (NumberFormatException e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            resp.sendError(400);
            objectMapper.writeValue(resp.getWriter(), "Wrong values");

            return;
        }
    }
}
