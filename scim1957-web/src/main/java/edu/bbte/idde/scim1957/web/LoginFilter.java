package edu.bbte.idde.scim1957.web;

import edu.bbte.idde.scim1957.backend.dao.mem.MenuMemDao;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

@WebFilter("/templateMenu")
public class LoginFilter extends HttpFilter {
    private static final Logger LOG = LoggerFactory.getLogger(MenuMemDao.class);

    @Override
    public void init() throws ServletException {
        LOG.info("Init filter");
    }

    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        LOG.info("Filter on login");
        HttpSession session = req.getSession(false);
        if (session == null || session.getAttribute("username") == null) {
            resp.sendRedirect(req.getContextPath().concat("/login"));
        } else {
            chain.doFilter(req, resp);
        }
    }
}
