<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="./design.css">
    <title>Menus</title>
</head>
<body>
    <div class = "container">
        <%@ page import="edu.bbte.idde.scim1957.backend.model.Menu"  %>
        <%@ page import="java.util.Collection" %>

        <table>
            <tr>
                <th>Id</th>
                <th>Price</th>
                <th>Name</th>
                <th>Calories</th>
                <th>Gluten Free</th>
                <th>Vegan</th>
            </tr>
            <%
            Collection<Menu> menu = (Collection<Menu>) request.getAttribute("menu");
            for (Menu entity: menu) {
            %>
            <tr>
                <td><%=entity.getId()%></td>
                <td><%=entity.getPrice()%></td>
                <td><%=entity.getName()%></td>
                <td><%=entity.getCalories()%></td>
                <td><%=entity.getGlutenFree()%></td>
                <td><%=entity.getVegan()%></td>
            </tr>
            <%}%>
        </table>
    </div>

    <form action="logout" method="POST">
        <input type="submit" value="Logout">
    </form>

</body>
</html>