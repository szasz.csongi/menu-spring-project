package edu.bbte.idde.scim1957.desktop;

import edu.bbte.idde.scim1957.backend.dao.ChefDao;
import edu.bbte.idde.scim1957.backend.dao.DaoFactory;
import edu.bbte.idde.scim1957.backend.dao.MenuDao;
import edu.bbte.idde.scim1957.backend.dao.RepositoryException;
import edu.bbte.idde.scim1957.backend.model.Chef;
import edu.bbte.idde.scim1957.backend.model.Menu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class HelloWorld {
    public static final Logger LOG = LoggerFactory.getLogger(HelloWorld.class);

    public static void main(String[] args) throws SQLException, RepositoryException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        classMenu(daoFactory);
        classChef(daoFactory);
    }

    private static void classMenu(DaoFactory daoFactory) throws RepositoryException, SQLException {
        MenuDao dao = daoFactory.getMenuDao();
        dao.create(new Menu(40, "Elso Fogas", 200.0, true, false));
        dao.create(new Menu(50, "Masodik Fogas", 250.23, false, false));

        for (Menu i:dao.findAll()) {
            LOG.info(i.toString());
        }

        Long id = 1L;
        Menu menu = dao.findById(id);
        LOG.info(menu.toString());

        dao.update(id, new Menu(60, "Harmadik Fogas", 300.0, true, true));
        menu = dao.findById(id);
        LOG.info(menu.toString());

        dao.delete(id);
        for (Menu i:dao.findAll()) {
            LOG.info(i.toString());
        }
    }

    private static void classChef(DaoFactory daoFactory) throws RepositoryException, SQLException {
        ChefDao dao = daoFactory.getChefDao();
        dao.create(new Chef("Bela", (long)1));
        dao.create(new Chef("Gyorgy", (long)2));

        for (Chef i:dao.findAll()) {
            LOG.info(i.toString());
        }

        Long id = 1L;
        Chef chef = dao.findById(id);
        LOG.info(chef.toString());

        dao.update(id, new Chef("Laszlo", (long)3));
        chef = dao.findById(id);
        LOG.info(chef.toString());

        dao.delete(id);
        for (Chef i:dao.findAll()) {
            LOG.info(i.toString());
        }
    }
}