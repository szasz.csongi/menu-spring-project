package edu.bbte.idde.scim1957.spring.dao.jpa;

import edu.bbte.idde.scim1957.spring.dao.ChefDao;
import edu.bbte.idde.scim1957.spring.model.Chef;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface ChefJpaDao extends JpaRepository<Chef, Long>, ChefDao {
    @Override
    default Chef update(Long id, Chef chef) {
        chef.setId(id);
        save(chef);
        flush();
        return chef;
    }
}
