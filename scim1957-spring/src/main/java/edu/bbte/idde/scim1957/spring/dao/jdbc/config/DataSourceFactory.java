package edu.bbte.idde.scim1957.spring.dao.jdbc.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("!mem")
public class DataSourceFactory {
    @Value("${jdbcUsername:root}")
    private String username;
    @Value("${jdbcPassword:root}")
    private String password;
    @Value("${jdbcUrl}")
    private String url;
    @Value("${jdbcDriver}")
    private String driver;

    @Bean
    public DataSource getDataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setMaximumPoolSize(10);
        hikariDataSource.setUsername(username);
        hikariDataSource.setPassword(password);
        hikariDataSource.setJdbcUrl(url);
        hikariDataSource.setDriverClassName(driver);

        return hikariDataSource;
    }
}
