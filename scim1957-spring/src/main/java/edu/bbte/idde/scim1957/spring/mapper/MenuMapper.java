package edu.bbte.idde.scim1957.spring.mapper;

import edu.bbte.idde.scim1957.spring.dto.in.MenuInDto;
import edu.bbte.idde.scim1957.spring.dto.out.MenuOutDto;
import edu.bbte.idde.scim1957.spring.model.Menu;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class MenuMapper {
    @Mapping(target = "id", ignore = true)
    public abstract Menu menuFromDto(MenuInDto menuDto);

    public abstract MenuOutDto dtoFromMenu(Menu menu);

    @IterableMapping(elementTargetType = MenuOutDto.class)
    public abstract Collection<MenuOutDto> dtosFromMenus(Collection<Menu> menus);
}
