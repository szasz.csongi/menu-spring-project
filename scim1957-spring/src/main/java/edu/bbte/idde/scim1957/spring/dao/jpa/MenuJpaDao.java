package edu.bbte.idde.scim1957.spring.dao.jpa;

import edu.bbte.idde.scim1957.spring.dao.MenuDao;
import edu.bbte.idde.scim1957.spring.model.Menu;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("jpa")
public interface MenuJpaDao extends JpaRepository<Menu, Long>, MenuDao {
    @Override
    default Menu update(Long id, Menu menu) {
        menu.setId(id);
        save(menu);
        flush();
        return menu;
    }
}
