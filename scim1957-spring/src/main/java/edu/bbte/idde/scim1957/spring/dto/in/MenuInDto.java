package edu.bbte.idde.scim1957.spring.dto.in;

import edu.bbte.idde.scim1957.spring.model.Chef;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Collection;

@Data
public class MenuInDto {
    @NotNull
    @Positive
    private Integer price;
    @NotNull
    @Size(max = 30)
    private String name;
    @NotNull
    @Positive
    private Double calories;
    @NotNull
    private Boolean glutenFree;
    @NotNull
    private Boolean vegan;
    Collection<Chef> chefs;
}
