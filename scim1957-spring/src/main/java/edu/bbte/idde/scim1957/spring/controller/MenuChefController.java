package edu.bbte.idde.scim1957.spring.controller;

import edu.bbte.idde.scim1957.spring.controller.exceptions.NotFoundException;
import edu.bbte.idde.scim1957.spring.dao.ChefDao;
import edu.bbte.idde.scim1957.spring.dao.MenuDao;
import edu.bbte.idde.scim1957.spring.dto.in.ChefInDto;
import edu.bbte.idde.scim1957.spring.dto.out.ChefOutDto;
import edu.bbte.idde.scim1957.spring.mapper.ChefMapper;
import edu.bbte.idde.scim1957.spring.model.Chef;
import edu.bbte.idde.scim1957.spring.model.Menu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@Profile("jpa")
@RestController
@RequestMapping("/menus")
@Slf4j
public class MenuChefController {
    @Autowired
    private MenuDao menuDao;
    @Autowired
    ChefDao chefDao;
    @Autowired
    private ChefMapper chefMapper;

    @GetMapping("/{id}/chefs")
    public Collection<ChefOutDto> findChefsByMenuId(@PathVariable("id") Long id) {
        log.info("Get chefs of menu: " + id);
        Menu menu = menuDao.getById(id);
        if (menu == null) {
            throw new NotFoundException("No menu found with this id: " + id);
        }

        Collection<Chef> chefs = menu.getChefs();
        return chefMapper.dtosFromChefs(chefs);
    }

    @PostMapping("/{id}/chefs")
    public ChefOutDto createChef(@PathVariable("id") Long id,
                                 @RequestBody @Valid ChefInDto chefInDto) {

        log.info("Post new chef to a menu nr: " + id);
        Menu menu = menuDao.getById(id);
        if (menu == null) {
            throw new NotFoundException("No menu found with this id: " + id);
        }
        Chef chef = chefMapper.chefFromDto(chefInDto);

        menu.getChefs().add(chef);
        menuDao.update(id,menu);
        return chefMapper.dtoFromChef(chef);
    }

    @DeleteMapping("/{id}/chefs")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteChef(@PathVariable("id") Long menuId, @RequestParam Long chefId) {
        log.info("Delete a Chef with id" + chefId + "from Menu" + menuId);
        Menu menu = menuDao.getById(menuId);

        if (menu == null) {
            throw new NotFoundException("No menu found with id =" + menuId);
        }

        if (chefDao.getById(chefId) == null) {
            throw new NotFoundException("No chef found with id =" + menuId);
        }

        Collection<Chef> chefs = menu.getChefs();

        Chef saveChef = null;

        for (Chef chef : chefs) {
            if (chef.getId().equals(chefId)) {
                saveChef = chef;
            }
        }

        if (saveChef == null) {
            throw new NotFoundException("No chef found with id in this menu =" + menuId);
        }

        chefs.remove(saveChef);

        menu.setChefs(chefs);
        menuDao.update(menuId,menu);
    }
}
