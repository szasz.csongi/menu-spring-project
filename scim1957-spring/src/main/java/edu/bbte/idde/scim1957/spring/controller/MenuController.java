package edu.bbte.idde.scim1957.spring.controller;

import edu.bbte.idde.scim1957.spring.controller.exceptions.NotFoundException;
import edu.bbte.idde.scim1957.spring.dao.MenuDao;
import edu.bbte.idde.scim1957.spring.dto.in.MenuInDto;
import edu.bbte.idde.scim1957.spring.dto.out.MenuOutDto;
import edu.bbte.idde.scim1957.spring.model.Menu;
import edu.bbte.idde.scim1957.spring.mapper.MenuMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/menus")
@Slf4j
public class MenuController {
    @Autowired
    private MenuDao menuDao;
    @Autowired
    private MenuMapper menuMapper;

    @GetMapping
    public Collection<MenuOutDto> findall(@RequestParam(required = false) String name) {
        if (name == null) {
            log.info("All menus");
            return menuMapper.dtosFromMenus(menuDao.findAll());
        }
        log.info("Menus with the given name");
        return menuMapper.dtosFromMenus(menuDao.findByName(name));
    }

    @GetMapping("/{id}")
    public MenuOutDto findById(@PathVariable("id") Long id) {
        Menu menu = menuDao.getById(id);
        log.info("Search menu by id");
        if (menu == null) {
            throw new NotFoundException("No menu with this id: " + id);
        }
        return menuMapper.dtoFromMenu(menu);
    }

    @PostMapping
    public MenuOutDto create(@RequestBody @Valid MenuInDto menuInDto) {
        log.info("Create new menu");
        Menu menu = menuMapper.menuFromDto(menuInDto);
        return menuMapper.dtoFromMenu(menuDao.saveAndFlush(menu));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMenu(@PathVariable("id") Long id) {
        log.info("Delete menu with this id: " + id);
        Menu menu = menuDao.getById(id);
        if (menu == null) {
            throw new NotFoundException("No menu with this id: " + id);
        }
        menuDao.deleteById(id);
    }

    @PutMapping("/{id}")
    public MenuOutDto updateMenu(@PathVariable("id") Long id,
                                 @RequestBody @Valid MenuInDto menuInDto) {
        log.info("Update menu with id: " + id);
        Menu oldMenu = menuDao.getById(id);
        if (oldMenu == null) {
            throw new NotFoundException("No menu with this id: " + id);
        }

        Menu newMenu = menuMapper.menuFromDto(menuInDto);
        return menuMapper.dtoFromMenu(menuDao.update(id,newMenu));

    }
}
