package edu.bbte.idde.scim1957.spring.controller;

import edu.bbte.idde.scim1957.spring.controller.exceptions.NotFoundException;
import edu.bbte.idde.scim1957.spring.dao.ChefDao;
import edu.bbte.idde.scim1957.spring.dao.MenuDao;
import edu.bbte.idde.scim1957.spring.dto.out.ChefOutDto;
import edu.bbte.idde.scim1957.spring.mapper.ChefMapper;
import edu.bbte.idde.scim1957.spring.model.Chef;
import edu.bbte.idde.scim1957.spring.model.Menu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@Profile("jpa")
@RestController
@RequestMapping("/chefs")
@Slf4j
public class ChefController {
    @Autowired
    MenuDao menuDao;

    @Autowired
    ChefDao chefDao;

    @Autowired
    ChefMapper chefMapper;

    @GetMapping
    public Collection<ChefOutDto> findAll() {
        log.info("All chefs");
        return chefMapper.dtosFromChefs(chefDao.findAll());
    }

    @GetMapping("/{id}")
    public ChefOutDto findById(@PathVariable("id") Long id) {
        Chef chef = chefDao.getById(id);
        log.info("Search chef by id");
        if (chef == null) {
            throw new NotFoundException("No menu with this id: " + id);
        }
        return chefMapper.dtoFromChef(chef);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteChef(@PathVariable("id") Long menuId) {
        log.info("Delete Chefs from Menu" + menuId);
        Menu menu = menuDao.getById(menuId);

        if (menu == null) {
            throw new NotFoundException("No menu found with id =" + menuId);
        }

        Collection<Chef> chefs = menu.getChefs();
        log.info(chefs.toString());

        for (Chef chef : chefs) {
            log.info(chef.getId().toString());
            chefDao.deleteById(chef.getId());
        }
    }
}
