package edu.bbte.idde.scim1957.spring.dao.jdbc;

import edu.bbte.idde.scim1957.spring.dao.MenuDao;
import edu.bbte.idde.scim1957.spring.model.Menu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
@Slf4j
@Profile("jdbc")
public class MenuJdbcDao implements MenuDao {
    @Autowired
    private DataSource dataSource;

    @Override
    public Menu saveAndFlush(Menu menu) {
        try {
            Connection connection = dataSource.getConnection();
            try {
                log.info("create {} - MenuJdbcDao", menu.toString());
                PreparedStatement preSta = connection.prepareStatement(
                        "INSERT into menus (price,name,calories,glutenFree,vegan) VALUES (?,?,?,?,?)");
                preSta.setInt(1, menu.getPrice());
                preSta.setString(2, menu.getName());
                preSta.setDouble(3, menu.getCalories());
                preSta.setBoolean(4, menu.getGlutenFree());
                preSta.setBoolean(5, menu.getVegan());
                preSta.executeUpdate();
            } catch (SQLException e) {
                log.error("MenuJdbcDao create failed");
            }
        } catch (SQLException e) {
            log.error("Connecion was not successful");
        }
        return menu;
    }

    @Override
    public Collection<Menu> findAll() {
        Collection<Menu> menus = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();

            try {
                log.info("findall - MenuJdbcDao");
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,price,name,calories,glutenFree,vegan FROM menus");
                ResultSet set = preSta.executeQuery();
                while (set.next()) {
                    Menu menu = createEntity(set);
                    menus.add(menu);
                }
                return menus;
            } catch (SQLException e) {
                log.error("Error in findall");
            }

        } catch (SQLException e) {
            log.error("Connecion was not successful");
        }
        return menus;
    }

    @Override
    public Menu getById(Long id) {
        try {
            Connection connection = dataSource.getConnection();
            try {
                log.info("findById {} - MenuJdbcDao", id);
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,price,name,calories,glutenFree"
                                + ",vegan FROM menus WHERE id=?");

                preSta.setLong(1,id);
                ResultSet set = preSta.executeQuery();
                if (set.next()) {
                    return createEntity(set);
                }
            } catch (SQLException e) {
                log.error("Hiba: {}", e.toString());
            }
        } catch (SQLException e) {
            log.error("Connecion was not successful");
        }
        return null;
    }

    @Override
    public void deleteById(Long id) {
        try {
            Connection connection = dataSource.getConnection();
            try {
                log.info("delete {} - MenuJdbcDao", id);
                PreparedStatement preSta =
                        connection.prepareStatement("DELETE FROM menus WHERE id=?");

                preSta.setLong(1,id);

            } catch (SQLException e) {
                log.error("Error in delete");
            }
        } catch (SQLException e) {
            log.error("Connecion was not successful");
        }
    }

    @Override
    public Menu update(Long id, Menu menu) {
        try {
            Connection connection = dataSource.getConnection();
            try {
                log.info("update {} - MenuJdbcDao", id);
                PreparedStatement preSta =
                        connection.prepareStatement("UPDATE menus SET price=?, name=?, calories=?, "
                                + "glutenFree=?, vegan=? WHERE id=?");
                preSta.setInt(1, menu.getPrice());
                preSta.setString(2, menu.getName());
                preSta.setDouble(3, menu.getCalories());
                preSta.setBoolean(4, menu.getGlutenFree());
                preSta.setBoolean(5, menu.getVegan());
                preSta.setLong(6,id);
                preSta.executeUpdate();
            }  catch (SQLException e) {
                log.error("Error in update");
            }
        } catch (SQLException e) {
            log.error("Connecion was not successful");
        }
        return menu;
    }

    @Override
    public List<Menu> findByName(String name) {
        List<Menu> menus = new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            log.info("Connection was successful");
            try {
                log.info("findByName {} - MenuJdbcDao", name);
                PreparedStatement preSta =
                        connection.prepareStatement("SELECT id,price,name,calories,glutenFree,vegan"
                                + " FROM menus WHERE name=?");
                preSta.setString(1,name);
                ResultSet set = preSta.executeQuery();
                if (set.next()) {
                    Menu menu = createEntity(set);

                    menus.add(menu);
                }
            } catch (SQLException e) {
                log.error("Error in find by name");
            }
            return menus;
        } catch (SQLException e) {
            log.error("Connecion was not successful");
        }
        return menus;
    }

    private Menu createEntity(ResultSet set) throws SQLException {
        Menu menu = new Menu(set.getInt(2), set.getString(3),
                set.getDouble(4), set.getBoolean(5), set.getBoolean(6),null);
        menu.setId(set.getLong(1));
        return menu;
    }
}
