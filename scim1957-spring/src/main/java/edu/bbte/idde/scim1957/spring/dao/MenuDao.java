package edu.bbte.idde.scim1957.spring.dao;

import edu.bbte.idde.scim1957.spring.model.Menu;

import java.util.List;

public interface MenuDao extends Dao<Menu> {
    List<Menu> findByName(String name);
}
