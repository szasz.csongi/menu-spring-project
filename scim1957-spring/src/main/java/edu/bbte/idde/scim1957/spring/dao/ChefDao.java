package edu.bbte.idde.scim1957.spring.dao;

import edu.bbte.idde.scim1957.spring.model.Chef;

public interface ChefDao extends Dao<Chef> {

}
