package edu.bbte.idde.scim1957.spring.mapper;

import edu.bbte.idde.scim1957.spring.dto.in.ChefInDto;
import edu.bbte.idde.scim1957.spring.dto.out.ChefOutDto;
import edu.bbte.idde.scim1957.spring.model.Chef;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(componentModel = "spring")
public abstract class ChefMapper {
    @Mapping(target = "id", ignore = true)
    public abstract Chef chefFromDto(ChefInDto chefInDto);

    public abstract ChefOutDto dtoFromChef(Chef chef);

    @IterableMapping(elementTargetType = ChefOutDto.class)
    public abstract Collection<ChefOutDto> dtosFromChefs(Collection<Chef> chefs);
}
