package edu.bbte.idde.scim1957.spring.dao.mem;

import edu.bbte.idde.scim1957.spring.dao.MenuDao;
import edu.bbte.idde.scim1957.spring.model.Menu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
@Profile("mem")
@Slf4j
public class MenuMemDao implements MenuDao {
    private final ConcurrentHashMap<Long, Menu> menus;
    private final AtomicLong id;

    public MenuMemDao() {
        menus = new ConcurrentHashMap<>();
        id = new AtomicLong();
        log.info("menuMemDao");
    }

    @Override
    public Menu saveAndFlush(Menu menu) {
        Long id = this.id.incrementAndGet();
        menu.setId(id);
        this.menus.put(id,menu);
        log.info("menu {} was created", menu);
        return menu;
    }

    @Override
    public Menu getById(Long id) {
        Menu menu = menus.get(id);
        log.info("Menu with id {} was found: {}", id, menu);
        return  menu;
    }

    @Override
    public Menu update(Long id, Menu menu) {
        menu.setId(id);
        menus.put(id, menu);
        log.info("Menu with id {} was updated to {}", id, menu);
        return menu;
    }

    @Override
    public void deleteById(Long id) {
        Menu menu = menus.get(id);
        if (menu != null) {
            menus.remove(id);
            log.info("Menu with id {} was deleted", id);
        }
    }

    @Override
    public Collection<Menu> findAll() {
        log.info("All menus from database were found");
        return menus.values();
    }

    @Override
    public List<Menu> findByName(String name) {
        log.info("finding menu by name {}", name);
        return menus.values().stream()
                .filter(menu -> menu.getName().equals(name))
                .collect(Collectors.toList());
    }
}
