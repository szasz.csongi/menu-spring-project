package edu.bbte.idde.scim1957.spring.dto.in;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ChefInDto {
    @NotNull
    @Size(max = 50)
    private String name;
}
