package edu.bbte.idde.scim1957.spring.dao;

import edu.bbte.idde.scim1957.spring.model.BaseEntity;

import java.util.Collection;

public interface Dao<T extends BaseEntity> {
    T saveAndFlush(T entity);

    Collection<T> findAll();

    T getById(Long id);

    void deleteById(Long id);

    T update(Long id, T entity);
}
