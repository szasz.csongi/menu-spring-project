package edu.bbte.idde.scim1957.spring.controller.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@EqualsAndHashCode(callSuper = false)
@Data
@AllArgsConstructor
public class NotFoundException extends RuntimeException {
    private String message;
}
